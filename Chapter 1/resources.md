## virtualenvwrapper URLs

https://virtualenvwrapper.readthedocs.org/en/stable/
https://pypi.python.org/pypi/virtualenvwrapper
https://bitbucket.org/dhellmann/virtualenvwrapper/

## For Windows users:

### virtualenvwrapper-win

A port of virtualenvwrapper to Windows batch scripts

https://pypi.python.org/pypi/virtualenvwrapper-win
https://github.com/davidmarble/virtualenvwrapper-win

### virtualenvwrapper-powershell

A port of virtualenvwrapper for Windows powershell

https://pypi.python.org/pypi/virtualenvwrapper-powershell
https://bitbucket.org/guillermooo/virtualenvwrapper-powershell
